cd ..

#clone the game engine
git clone https://gitlab.com/nag_personal_projects/terminal_engine
cd terminal_engine

#build the engine
mkdir build
cd build
cmake ..
make -j8

cd ../../race_game

#build the game
mkdir build
cd build
cmake ..
make -j8
