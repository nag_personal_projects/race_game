#include "car_spawner.hpp"
#include "game_rules.hpp"
#include "car.hpp"

#include <ctime>

#include <scene.hpp>
#include <timer.hpp>
#include <event_system.hpp>

CarSpawner::CarSpawner(GameRules* rules, const ScenePtr& scene, uint64_t time) :
    m_scene(scene),
    m_rules(rules),
    m_timer(new TE::Timer(time, [this](){this->spawnCar();}, true, false)) {}

CarSpawner::~CarSpawner() {
    delete m_timer;
}

void CarSpawner::run() {
    m_timer->start();
}

void CarSpawner::stop() {
    m_timer->stop();
}

void CarSpawner::spawnCar() {
    static int lines[4] = {1, 6, 11, 16};

    const int randomLineIndex = rand() % 4;
    const int randomLine = lines[randomLineIndex];

    auto car = std::make_shared<Car>(4, 4, randomLine, 0);
    car->appendTag("car");

    car->getEventSystem()->appendSubscriber(m_rules);

    m_scene->appendObject(car);
}