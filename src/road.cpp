#include "road.hpp"

#include <canvas.hpp>

Road::Road(int w, int h, int s) :
    TE::Object(w, h, 0, 0)
{
    m_step = s;
}

void Road::onDestroy() {}

void Road::onUpdate(clock_t FPS) {}

void Road::onDraw(const CanvasPtr& canvas) {
    const int w = canvas->getWidth();
    const int h = canvas->getHeight();

    for (int i = 0; i < w; i += m_step + 1) {
        for (int j = 0; j < h; ++j) {
            canvas->at(i, j) = '|';
        }
    }
}

void Road::onIntersects(const ObjectPtr& object) {}