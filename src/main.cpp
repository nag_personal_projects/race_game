#include <iostream>
#include <memory>

#include <engine.hpp>
#include <scene.hpp>
#include <object.hpp>
#include <keyboard.hpp>

#include "road.hpp"
#include "car.hpp"
#include "car_spawner.hpp"
#include "player.hpp"
#include "game_rules.hpp"

typedef std::shared_ptr<TE::Engine> EnginePtr;
typedef std::shared_ptr<TE::Scene> ScenePtr;
typedef std::shared_ptr<TE::Object> ObjectPtr;

int main() {
    srand(time(nullptr));

    ScenePtr scene = std::make_shared<TE::Scene>(21, 40);

    ObjectPtr road = std::make_shared<Road>(21, 40, 4);
    scene->appendObject(road);

    auto player = std::make_shared<Player>(4, 4, 1, 36);
    scene->appendObject(player);

    EnginePtr engine = std::make_shared<TE::Engine>();
    engine->setScene(scene);
    engine->setLimitFPS(24);
    engine->limitFPS(true);

    auto rules = std::make_shared<GameRules>(engine.get(), player.get());
    auto spawner = std::make_shared<CarSpawner>(rules.get(), scene, 1);

    engine->run();
    spawner->run();
	
    while (!TE::Keyboard::isKeyPressed(XK_Escape) && !rules->gameIsOver());

	engine->stop();
    spawner->stop();

    if (rules->gameIsOver()) {
        std::cout << "Game Over" << std::endl;
        std::cout << "Your score is " << rules->getScore() << std::endl;
    }

    return 0;
}