#include "player.hpp"

#include <canvas.hpp>
#include <keyboard.hpp>

Player::Player(int w, int h, int x, int y) :
    Car(w, h, x, y)
{
    m_lastKeyReleased = true;
}

void Player::onUpdate(clock_t FPS) {
    if (!m_lastKeyReleased) {
        if (TE::Keyboard::isKeyReleased(XK_Left) || TE::Keyboard::isKeyReleased(XK_Right)) {
            m_lastKeyReleased = true;
        }

        return;
    }

    if (TE::Keyboard::isKeyPressed(XK_Left) && m_posX > 1) {
        m_posX -= 5;
        m_lastKeyReleased = false;
    }

    if (TE::Keyboard::isKeyPressed(XK_Right) && m_posX < 15) {
        m_posX += 5;
        m_lastKeyReleased = false;
    }
}

void Player::onDraw(const CanvasPtr& canvas) {
    for (int i = m_posX; i < m_width + m_posX; ++i) {
        for (int j = m_posY; j < m_height + m_posY; ++j) {
            canvas->at(i, j) = '*';
        }
    }
}

void Player::onIntersects(const ObjectPtr& object) {
    if (object->hasTag("car")) {
        destroy();
    }
}