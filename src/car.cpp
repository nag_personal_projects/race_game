#include "car.hpp"

#include <canvas.hpp>

Car::Car(int w, int h, int x, int y) :
    TE::Object(w, h, x, y) {}

void Car::onDestroy() {}

void Car::onUpdate(clock_t FPS) {
    m_posY += 0.5;

    if (m_posY >= 40) {
        destroy();
    }
}

void Car::onDraw(const CanvasPtr& canvas) {
    for (int i = m_posX; i < m_width + m_posX; ++i) {
        for (int j = m_posY; j < m_height + m_posY; ++j) {
            canvas->at(i, j) = '*';
        }
    }
}

void Car::onIntersects(const ObjectPtr& object) {}