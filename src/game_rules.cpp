#include "game_rules.hpp"
#include "player.hpp"

#include <engine.hpp>
#include <event_system.hpp>
#include <scene.hpp>

#include <stdio.h>

GameRules::GameRules(TE::Engine* engine, Player* player) :
    TE::Object(0, 0, 0, 0),
    m_engine(engine),
    m_player(player)
{
    if (player) {
        m_player->getEventSystem()->appendSubscriber(this);
    } else {
        throw("error: player is nullptr");
    }

    m_score = 0;
    m_over = false;
}

int GameRules::getScore() const {
    return m_score;
}

bool GameRules::gameIsOver() const {
    return m_over;
}

void GameRules::onDestroy() {}

void GameRules::onUpdate(clock_t FPS) {}

void GameRules::onDraw(const CanvasPtr& canvas) {}

void GameRules::onIntersects(const ObjectPtr& object) {}

void GameRules::onEvent(const EventPtr& event) {
    if (event->object == m_player) {
        m_over = true;
    }

    if (event->object->hasTag("car") == true) {
        m_score += 100;
    }
}