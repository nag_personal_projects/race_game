#ifndef ROAD_HPP
#define ROAD_HPP

#include <object.hpp>

#include <memory>

namespace TE {
    class Canvas;
}

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;

class Road : public TE::Object {

    int m_step;

    public:

        Road(int w, int h, int s);
        ~Road() override = default;

    protected:

        void onDestroy() override;

        void onUpdate(clock_t FPS) override;
        void onDraw(const CanvasPtr& canvas) override;
        void onIntersects(const ObjectPtr& object) override;

};

#endif //!ROAD_HPP