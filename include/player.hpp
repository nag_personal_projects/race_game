#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "car.hpp"

class Player : public Car {

    bool m_lastKeyReleased;

public:

    Player(int w, int h, int x, int y);
    ~Player() = default;

protected:


    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

};

#endif //!PLAYER_HPP