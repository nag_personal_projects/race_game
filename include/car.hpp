#ifndef CAR_HPP
#define CAR_HPP

#include <memory>

#include <object.hpp>

namespace TE {
    class Canvas;
}

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;

class Car : public TE::Object {

public:

    Car(int w, int h, int x, int y);
    ~Car() override = default;

protected:

    void onDestroy() override;

    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

};

#endif //!CAR_HPP