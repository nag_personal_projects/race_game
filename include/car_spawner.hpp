#ifndef CAR_SPAWNER_HPP
#define CAR_SPAWNER_HPP

#include <memory>
#include <stdint.h>

namespace TE {
    class Timer;
    class Scene;
}

class GameRules;

typedef std::shared_ptr<TE::Scene> ScenePtr;

class CarSpawner {

    TE::Timer* m_timer;
    ScenePtr m_scene;

    GameRules* m_rules;

public:

    CarSpawner(GameRules* rules, const ScenePtr& scene, uint64_t time);
    ~CarSpawner();

    void run();
    void stop();

private:

    void spawnCar();

};

#endif //!CAR_SPAWNER_HPP