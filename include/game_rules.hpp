#ifndef GAME_RULES_HPP
#define GAME_RULES_HPP

#include "object.hpp"

class Player;

namespace TE {
    class Engine;
    class Canvas;
    class Event;
};

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;
typedef std::shared_ptr<TE::Event> EventPtr;

class GameRules : public TE::Object {

    TE::Engine* m_engine;
    Player* m_player;

    int m_score;

    bool m_over;

public:

    GameRules(TE::Engine* engine, Player* player);
    ~GameRules() override = default;

    int getScore() const;
    bool gameIsOver() const;

protected:

    void onDestroy() override;

    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

    void onEvent(const EventPtr& event) override;

};

#endif //!GAME_RULES_HPP